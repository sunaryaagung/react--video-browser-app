import axios from "axios";

const KEY = "AIzaSyA8YRgPLyoukw09EvRouYTSXW_bfF6SmzM";

export default axios.create({
  baseURL: "https://www.googleapis.com/youtube/v3",
  params: {
    part: "snippet",
    maxResult: 5,
    key: KEY
  }
});
